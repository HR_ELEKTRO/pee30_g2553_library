/**
 *  @file i2c.h
 *  @brief Dit headerbestand bevat alle i2c prototypes
 *
 *  @author VersD
 *  @date   4 mei 2017
 */

#ifndef _I2C_H
#define _I2C_H
/**
 * \defgroup I2C
 * @brief I2C functionaliteiten
 *
 * De Universal Serial Communication Interface (USCI) module kan draaien in 3 modi:
 * I2C, SPI en UART. Met deze functies kun je de module in I2C modus gebruiken.
 *
 * #Master modus
 * De master modus werkt niet op basis van interrupts, maar heeft blokkerende functies.
 *
 * De volgende stappen moeten worden ondernomen om i2c te kunnen gebruiken als master:
 * + Intialiseer de module met i2cInitialize()
 * + Stel modus in op MASTER met i2cSetMode()
 * + Stel het I2C adres in van de slave met i2cSetAddress()
 * + Stuur data op met i2cSendBytes()
 *
 * # Slave modus:
 * De slave modus werkt wel op basis van interrupts.
 *
 * Stel de volgende dingen in:
 * + Stel modus in op SLAVE met i2cSetMode()
 * + Stel het I2C adres van de MSP met i2cSetOwnAddress()
 * + Definieer twee functies: void i2cSlaveTransmitISR(void ) en  void i2cSlaveReceiveISR(void). Binnen deze functies wordt alles verwerkt.
 *
 *
 * @{
 *
 */
#include "clocks.h"
#include <stdint.h>

//!De twee i2c modi
typedef enum {MASTER,SLAVE} i2cMode;
//!Standaard aan/uit toestand
typedef enum {ENABLE=1,DISABLE=0} status;

/*!
 * @brief Wanneer een byte wordt verwacht door de i2c master wordt deze interrupt aangeroepen.
 *
 * Deze functie moet je zelf in je code implementeren
 *
 * @code
 * UCB0TXBUF = counter; //stuur counter naar de master
 * @endcode
 */
extern void i2cSlaveTransmitISR(void);

/*!
 * @brief Wanneer een byte wordt gestuurd door de i2c master wordt deze interrupt aangeroepen.
 *
 * Deze functie moet je zelf in je code implementeren. De functie wordt aangeroepen (interrupt)
 * zodra de volgende byte binnen komt van de master. Als de master 3 bytes stuurt, wordt deze
 * interrupt drie maal aangeroepen.
 *
 * @code
 * //Zet elke nieuwe ontvangen byte in een array
 * i2cRxData[i2cByteCounter++] = UCB0RXBUF;
 *
 * //In de main code kun je controleren op het STOP vlaggetje
 * while(!(UCB0STAT & UCSTPIFG)); //zolang geen stopconditie heeft plaatsgevonden....
 * @endcode
 */
extern void i2cSlaveReceiveISR(void);

//public functions
/*!
 * @brief Stel de USCI module in voor I2C gebruik (i.p.v. SPI)
 *
 * \param mode Stel de i2c module in als SLAVE of MASTER. @see i2cSetMode()
 */
void i2cInitialize(i2cMode mode);

/*!
 * @brief Stel hiermee in of de module MASTER of SLAVE is op de i2c bus.
 *
 * @code
 * i2cSetMode(MASTER)
 * @endcode
 *
 * \param mode Stel de i2c module in als SLAVE of MASTER.
 * @todo Implementeer slave modus
 */
void i2cSetMode(i2cMode mode);

/*!
 * @brief Stel het adres in van de andere i2c module
 *
 * Elke i2c module heeft een uniek adres op bus.
 *
 * \param address Het 7 bits adres zonder read/write bit, volledig naar rechts geschoven. Dus bits 0-6 bevatten het adres.
 */
void i2cSetAddress(uint8_t address);


/*!
 * @brief Stel het eigen adres in van de msp430 i2c peripheral
 *
 * Wanneer de msp430 als slave wordt ingesteld moet deze een adres hebben.
 *
 * \param address Het 7 bits adres zonder read/write bit, volledig naar rechts geschoven. Dus bits 0-6 bevatten het adres.
 */
void i2cSetOwnAddress(uint8_t address);
/*!
 * @brief Zet een databyte in de transmit-buffer.
 *
 * Deze functie roept geen start of stop conditie aan. Het is dus te gebruiken wanneer
 * data verstuurt moet worden die run-time gegenereerd wordt. Zorg er dan wel voor dat
 * de communicatie wordt gestart en gestopt @see i2cStart @see i2cStop
 *
 * \param byte De 8 bits data om te laten versturen.
 */
void i2cSendByte(uint8_t byte);

/*!
 * @brief Start verbinding en stuur data
 *
 * Met deze functie kun je meerdere databytes versturen. De functie zet de verbinding
 * op stand door een startconditie te versturen, stuurt vervolgens alle meegegeven
 * data en eindigt met een stopconditie.
 *
 * @code
 * uint8_t data = {0x0A, 0xAC, 0xFF, 0x12};
 * i2cSendBytes(4, data);
 * @endcode
 *
 * \param number Het aantal bytes wat wordt verstuurd
 * \param bytes Een array met de te versturen bytes
 */
void i2cSendBytes(uint8_t number, const uint8_t bytes[]);

/*!
 * @brief Stel de i2c klok in (wanneer in MASTER modus)
 *
 * In Master modus genereert de microcontroller de i2c-klok zelf.
 * Stel met deze functie in van welke klokbron de module moet draaien
 * en met welke waarde deze klok nog gedeeld moet worden. De uitkomst van
 * deze deling is de uiteindelijk gebruikte klokfrequentie.
 *
 * \param source De klokbron waarop de module zal draaien.
 * \param divider Deel de klokfrequentie door een 16 bits integer waarde.
 */
void i2cSetClock(clockSources source, uint16_t divider);

/*!
 * @brief Stuur een startconditie
 */
void i2cStart();

/*!
 * @brief Stuur een stopconditie
 */
void i2cStop();

/**
 * @}
 */
#endif
